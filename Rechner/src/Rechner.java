import java.util.Scanner;

public class Rechner {

	public static void main(String[] args) {
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl an: ");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis_addition = zahl1 + zahl2;
		int ergebnis_multiplikation = zahl1 * zahl2;
		int ergebnis_division = zahl1 / zahl2;
		int ergebnis_subthration = zahl1 - zahl2;
		
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis_addition);
		System.out.print("Ergebnis der Multiplikation lautet: ");
		System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis_multiplikation);
		System.out.print("Ergebnis der Division lautet: ");
		System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnis_division);
		System.out.print("Ergebnis der Subthration lautet: ");
		System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnis_subthration);
		
		myScanner.close();
		
	}
}
