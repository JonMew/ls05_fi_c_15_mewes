import java.util.Scanner;

public class Aufgabe_A_2_3 {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Guten Morgen");
		
		System.out.println("Wie hei�t du?");
		String name = myScanner.next();
		
		System.out.println("Wie alt bist du?");
		int alter = myScanner.nextInt();
		
		System.out.println("Du bist also " + alter + " Jahre alt und dein Name ist " + name + ".");
		
		myScanner.close();
	}

}
