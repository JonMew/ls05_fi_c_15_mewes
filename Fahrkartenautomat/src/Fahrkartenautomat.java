﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);

    }
    

    public static double fahrkartenbestellungErfassen() 
    {
 	   Scanner tastatur = new Scanner(System.in);
	   
 	   double zuZahlenderBetrag;
 	   short anzahlTickets;
 	   double zuZahlenderGesamtbetrag;
 	   
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
        if (zuZahlenderBetrag <= 0 || zuZahlenderBetrag > 10) {
        	System.out.println("Fehler, ungültiger Wert daher wird automatisch 1€ als Preis gewählt.");
        	zuZahlenderBetrag = 1;
        	System.out.println("Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextShort();
        }
        else {
        	System.out.println("Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextShort();
        }
        if (anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.println("Fehler, ungültiger Wert daher wird automatisch '1' Ticket ausgewählt.");
        	anzahlTickets = 1;
        	zuZahlenderGesamtbetrag = zuZahlenderBetrag * anzahlTickets;
        }
        else {
        	zuZahlenderGesamtbetrag = zuZahlenderBetrag * anzahlTickets;
        }
        
        return zuZahlenderGesamtbetrag;
    }

	public static double fahrkartenBezahlen(double zuZahlenderGesamtbetrag) 
	{
	    Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
	    double eingeworfeneMünze;
	    double rückgabebetrag;
	    
		// Geldeinwurf
	    // -----------
	    eingezahlterGesamtbetrag = 0.00;
	    while(eingezahlterGesamtbetrag < zuZahlenderGesamtbetrag)
	    {
	    	System.out.printf("Noch zu zahlen: " +  "%.2f", (zuZahlenderGesamtbetrag - eingezahlterGesamtbetrag));
	    	System.out.println(" Euro");
	    	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	eingeworfeneMünze = tastatur.nextDouble();
	        eingezahlterGesamtbetrag += eingeworfeneMünze;
	    }
	    rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderGesamtbetrag;
	    tastatur.close();
	    return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() 
	{
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
	          	} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	       
	}
	
	public static void rueckgeldAusgeben(double rückgabebetrag) 
	{
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	 	          
	           }
	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
	    		   			"vor Fahrtantritt entwerten zu lassen!\n" +
	    		   			"Wir wünschen Ihnen eine gute Fahrt.");
	       
		   	
	}
      

	
       /* es werden folgende Operationen ausgeführt: 
        * 
        * der zu zahlende Betrag wird * die Tickets gerechnet
        * solange der eingezahlte Gesamtbetrag kleiner als der eingezahlte Betrag ist wird dieser vom zu zahlenden betrag abgezogen
        * eingezahlte Betrag wird mit dem Gesamt eingezahlten Betrag zusammen gerechnet
        * der eingezahlte Gesamtbetrag wird vom Gesamtbetrag der Kosten abgezogen und als Rückgabebetrag ausgezaht
        * 
       */
}
