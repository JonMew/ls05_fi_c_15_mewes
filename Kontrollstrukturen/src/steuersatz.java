import java.util.Scanner;

public class steuersatz {
	public static void main(String[] args) {

		Steuersatz();
	}

	public static void Steuersatz() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben Sie den Nettowert ein: ");
		double Nettowert = tastatur.nextDouble();

		System.out.println(
				"Soll hier der erm��igte Steuersatz berechnet werden dann dr�cken Sie 'j', ansonsten f�r den vollen Steuersatz 'n'.");
		char welcherSteuersatz = tastatur.next().charAt(0);

		if (welcherSteuersatz == 'j') {
			double bruttoErmae�igt = Nettowert * 1.07;
			System.out.printf("Der Bruttowert erm��igt betr�gt: " + "%.2f�", bruttoErmae�igt);
		}

		else if (welcherSteuersatz == 'n') {
			double bruttoVoll = Nettowert * 1.19;
			System.out.printf("Der Bruttowert mit dem vollen Steuersatz betr�gt: " + "%.2f�", bruttoVoll);
		}

		else {
			System.out.println("Fehler bitte mit 'j' oder 'n' antworten!");
		}
		tastatur.close();
	}
}
