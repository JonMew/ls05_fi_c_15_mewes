import java.util.Scanner;

public class aufgabe2 {
	public static void main(String [] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein: ");
		int a = tastatur.nextInt();
		System.out.println("Geben Sie eine weitere Zahl ein: ");
		int b = tastatur.nextInt();
		System.out.println("Geben Sie eine dritte Zahl ein: ");
		int c = tastatur.nextInt();
		
//		aufgabeEins(a, b, c);
//		aufgabeZwei(a, b, c);
		aufgabeDrei(a, b, c);
		
		
		tastatur.close();	
	}
	
	public static void aufgabeEins(int a, int b, int c) {
		if (a > b && a > c)
		{
			System.out.println("Die Erste Zahl ist gr��er als die anderen beiden.");
		}
	}
	
	public static void aufgabeZwei(int a, int b, int c) {
		if (c > a || c > b)
		{
			System.out.println("Die dritte Zahl ist gr��er als die erste ODER die zweite.");
		}
	}
	
	public static void aufgabeDrei(int a, int b, int c) {
		if (a > b && a > c)
		{
			System.out.println("Die erste Zahl ist die gr��te!");
		}
		else if (b > c)
		{
			System.out.println("Die zweite Zahl ist die gr��te!");
		}
		else
		{
			System.out.println("Die dritte Zahl ist die gr��te!");
		}
	}
}
