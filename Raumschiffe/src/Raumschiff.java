import java.util.ArrayList;

public class Raumschiff {

	  private int photonentorpedoAnzahl;
	  private int energieversorgungInProzent;
	  private int schildeInProzent;
	  private int huelleInProzent;
	  private int lebenserhaltungssystemeInProzent;
	  private int androidenAnzahl;
	  private String schiffsname;
	  private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	  private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	  
	  
	  
	  // Konstruktoren
	  public Raumschiff() {
	    
	  }
	  
	  public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,
	                int zustandSchildeInProzent, int zustandHuelleInProzent,
	                int zustandLebenserhaltungssystemeInProzent,
	                int anzahlDroiden, String schiffsname) {
	    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	    this.energieversorgungInProzent = energieversorgungInProzent;
	    this.schildeInProzent = zustandSchildeInProzent;
	    this.huelleInProzent = zustandHuelleInProzent;
	    this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	    this.androidenAnzahl = anzahlDroiden;
	    this.schiffsname = schiffsname;
	  }
	  
	  
	  // getter & setter
	  public int getPhotonentorpedoAnzahl() {
	    return photonentorpedoAnzahl;
	  }

	  public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
	    this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	  }

	  public int getEnergieversorgungInProzent() {
	    return energieversorgungInProzent;
	  }

	  public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
	    this.energieversorgungInProzent = energieversorgungInProzent;
	  }

	  public int getSchildeInProzent() {
	    return schildeInProzent;
	  }

	  public void setSchildeInProzent(int schildeInProzent) {
	    this.schildeInProzent = schildeInProzent;
	  }

	  public int getHuelleInProzent() {
	    return huelleInProzent;
	  }

	  public void setHuelleInProzent(int huelleInProzent) {
	    this.huelleInProzent = huelleInProzent;
	  }

	  public int getLebenserhaltungssystemeInProzent() {
	    return lebenserhaltungssystemeInProzent;
	  }

	  public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
	    this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	  }

	  public int getAndroidenAnzahl() {
	    return androidenAnzahl;
	  }

	  public void setAndroidenAnzahl(int androidenAnzahl) {
	    this.androidenAnzahl = androidenAnzahl;
	  }
	  public String getSchiffsname() {
	    return schiffsname;
	  }

	  public void setSchiffsname(String schiffsname) {
	    this.schiffsname = schiffsname;
	  }
	  
	  
	  // methoden
	  public void zustandRaumschiff() {
		  System.out.printf("Photonentorpedoanzahl: %s\n", getPhotonentorpedoAnzahl());
		  System.out.printf("Energieversorgung: %s\n", getEnergieversorgungInProzent());
		  System.out.printf("Schilde: %s\n", getSchildeInProzent());
		  System.out.printf("H�lle: %s\n", getHuelleInProzent());
		  System.out.printf("Lebenserhaltungssysteme: %s\n", getLebenserhaltungssystemeInProzent());
		  System.out.printf("Andoridenanzahl: %s\n", getAndroidenAnzahl());
		  System.out.printf("Schiffsname: %s\n", getSchiffsname());
	    
	  }
	  
	  public void printLadungsverzeichnis() {
		  for (Ladung ladung : ladungsverzeichnis) {
			  System.out.print(ladung.getBezeichnung());
			  System.out.print(", ");
			  System.out.println(ladung.getMenge());
		  }
	    
	  }
	  
	  public void addLadung(Ladung neueLadung) {
	    ladungsverzeichnis.add(neueLadung);
	  }
	  
	  public void photonentorpedoSchiessen(Raumschiff r) {
		  if (getPhotonentorpedoAnzahl() == 0) {
			  nachrichtAnAlle("-=*Click*=-");
		  }
		  else {
			  setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() -1);
			  nachrichtAnAlle("Photonentorpedo abgeschossen");
			  treffer(r);
		  }
	  
	  }
	  
	  public void phaserkanoneSchiessen(Raumschiff r) {
		  if (getEnergieversorgungInProzent() < 50) {
			  nachrichtAnAlle("-=*Click*=-");
		  }
		  else {
			  setEnergieversorgungInProzent(getEnergieversorgungInProzent() -50);
			  nachrichtAnAlle("Phaserkanone abgeschossen");
			  treffer(r);
		  }
		  
	    
	  }
	  
	  private void treffer(Raumschiff r) {
		  nachrichtAnAlle(r.getSchiffsname() + "wurde getroffen!");
	   
	  }
	  
	  public void nachrichtAnAlle(String message) {
		  System.out.println(message);
	  }
	  
	  /*public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
	    
	  } */
	  
	  public void photonentorpedosLaden(int anzahlTorpedos) {
	    
	  }
	  
	  public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
	                                      boolean schiffshuelle, int anzahlDroiden)  {
	    
	  }
	  
	  public void ladungsverzeichnisAusgeben() {
	    
	  }
	  
	  public void ladungsverzeichnisAufraeumen() {
	    
	  }
}

