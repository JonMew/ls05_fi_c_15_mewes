
public class Aufgabe3 {

	public static void main(String[] args) {
		// 22.4234234 111.2222 4.0 1000000.551 97.34
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.println("Folgende Zahlen werden mit zwei Nachkommastellen angezeigt");
		
		System.out.printf("\n%.2f", a);
		System.out.printf("\n%.2f", b);
		System.out.printf("\n%.2f", c);
		System.out.printf("\n%.2f", d);
		System.out.printf("\n%.2f", e);
		
		// "%.2f" gibt eine Zahl mit zwei Nachkommastellen an
	}

}
