
public class Aufgabe1 {
	public static void main(String[] args) {
		
		System.out.println("Aufgabe 1:\n");
		String s = "**";
		String t = "*";
		
		System.out.printf("%15s\n", s);
		System.out.printf("%11s %6s\n", t, t);
		System.out.printf("%11s %6s\n", t, t);
		System.out.printf("%15s\n\n", s);
		
		System.out.println("Aufagbe 2:\n");
		String n0 = "0!";
		String n1 = "1!";
		String n2 = "2!";
		String n3 = "3!";
		String n4 = "4!";
		String n5 = "5!";

		
		System.out.printf("%-5s = \t\t\t =%6d\n",n0, 1);
		System.out.printf("%-5s = 1\t\t\t =%6d\n",n1, 1);
		System.out.printf("%-5s = 1 * 2\t\t\t =%6d\n",n2, 2);
		System.out.printf("%-5s = 1 * 2 * 3\t\t =%6d\n",n3, 6);
		System.out.printf("%-5s = 1 * 2 * 3 * 4\t\t =%6d\n",n4, 24);
		System.out.printf("%-5s = 1 * 2 * 3 * 4 * 5\t =%6d\n\n",n5, 120);
		
		System.out.println("Aufgabe 3:\n");
		int f1 = -20;
		int f2 = -10;
		int f3 = 0;
		int f4 = 20;
		int f5 = 30;
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		System.out.println("Fahrenheit  |  Celsius");
		System.out.println("-----------------------");
		System.out.printf("%-12d|%10.2f\n", f1, c1 );
		System.out.printf("%-12d|%10.2f\n", f2, c2);
		System.out.printf("%-12d|%10.2f\n", f3, c3);
		System.out.printf("%-12d|%10.2f\n", f4, c4);
		System.out.printf("%-12d|%10.2f\n", f5, c5);
		
	}
}
