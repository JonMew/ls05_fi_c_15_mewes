
public class Volumenberechnung {
		public static void main(String[] args) {
			
			double a = 2;
			double b = 4;
			double c = 6;
			double r = 2;
			double W�rfVolumen = W�rfelVolumen(a);
			double QuadVolumen = QuaderVolumen(a, b, c);
			double PyramidVolumen = PyramidenVolumen(a, a, c);
			double KugVolumen = KugelVolumen(r);
			
			
		System.out.printf("Das Volumen eines W�rfel bei einer L�nge von " + a + " betr�gt " + W�rfVolumen + ".\n");
		System.out.printf("Das Volumen eines Quader bei einer L�nge von " + a + " und einer Breite von " + b + " und einer H�he von " + c + " betr�gt " + QuadVolumen + ".\n");
		System.out.printf("Das Volumen einer Pyramide bei einer L�nge und Breite von " + a + " und einer H�he von " + c + " betr�gt " + PyramidVolumen + ".\n");
		System.out.printf("Das Volumen einer Kugel bei einem Radius von " + r + " betr�gt " + KugVolumen + ".\n");
		
		}
		public static double W�rfelVolumen(double a) {
			return a * a * a;
		}
		public static double QuaderVolumen(double a, double b, double c) {
			return a * b * c;
		}
		public static double PyramidenVolumen(double a, double b, double c) {
			return a * a * c / 3;
		}
		public static double KugelVolumen(double r) {
			return 4/3 * (r * r * r) * 3.14;
		}

}
