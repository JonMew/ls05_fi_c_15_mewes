import java.util.Scanner;
public class Mittelwert {
		   public static void main(String[] args) {
			   

		      // (E) "Eingabe"
		      // Werte f�r x und y festlegen:
		      // ===========================
			 Scanner scanner = new Scanner(System.in); 
			 
			 double summe = 0;
			 int zaehler = 0;
			 
			 System.out.println("Geben Sie die Anzahl der Zahlen f�r die Mittelwert Berechnung an: ");
			 int anzahl = scanner.nextInt();
			 
			 while(zaehler < anzahl) {
				 System.out.println("Geben Sie eine Zahl ein: ");
				 double y = scanner.nextDouble();
				 summe = summe + y;
				 zaehler++;
			 }
			 
			 double mittelwert = summe / anzahl;
			 System.out.println("Der Mittelwert der Zahlen ist: " + mittelwert);

		      
		      scanner.close();
		   }
}


